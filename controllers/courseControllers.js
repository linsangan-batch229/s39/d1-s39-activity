const Course = require("../models/Course.js");
const auth = require('../auth.js');
const Users = require("../models/User.js");

//create a new course
/*
Steps:
1. create a new Course object using the mongosoe model
2. save the new Course to the database 
*/

module.exports.addCourse = (reqBody) => {

    //craetes a new variable "newCourse and instantiate a new  Course
    //object
    //uses the information from the reqbody to provide all necessary
    //info

    let newCourse = new Course({
        name: reqBody.name,
        description: reqBody.description,
        price: reqBody.price,
    })
 
    //saves the create object to our database
    return newCourse.save().then((course, error) => {
        // if failed
        if (error){
            return false;
        //if success
        } else {
            return `A new course has been created "${reqBody.description}."`;
        }
    })
}


