const express = require('express');
const router = express.Router();
const courseControllers = require("../controllers/courseControllers");
const auth = require("../auth.js")

//crete course
router.post("/", auth.verify, (req, res) => {

    //retrieve the all the data from user
    const userData = auth.decode(req.headers.authorization)
    
    //get the isAdmin key if its T/F
    if(userData.isAdmin){
        courseControllers.addCourse(req.body).then(resultFromController => res.send(resultFromController));

    } else {
        res.send(`Failed to register. User "${req.body.name}" is not an admin.`)
    }

   

});

//export the router object for index.jsfil
module.exports = router;