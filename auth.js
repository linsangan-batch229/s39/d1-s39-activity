const jwt = require('jsonwebtoken');
// [Section] JSON Web Tokens
		/*
		- JSON Web Token or JWT is a way of securely passing information from the server to the frontend or to other parts of server
		- Information is kept secure through the use of the secret code
		- Only the system that knows the secret code that can decode the encrypted information
- Imagine JWT as a gift wrapping service that secures the gift with a lock
		- Only the person who knows the secret code can open the lock
		- And if the wrapper has been tampered with, JWT also recognizes this and disregards the gift
		- This ensures that the data is secure from the sender to the receiver
		*/


//token creation
const secret = "CourseBookingAPI";

module.exports.createAccessToken = (user) => {
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}

	return jwt.sign(data, secret, {})
}

// TOken verification
/*
Analogy:
	recieve the gift and open it to verify if the sender if legitiamte
	and was not tempered

*/

//		[VERIFY USERS]
module.exports.verify = (req, res, next) => {

	// token is retrieved from the request header
	let token = req.headers.authorization;
	console.log(token);

	//token received and is not undefined
	if(typeof token !== "undefined"){
		console.log(token);

		//separating
		// "slice" method, 
		token = token.slice(7, token.length);
		
		// validate the token using the verify method decript using
		// the secret code
		// descrypting the token using the secret
		return jwt.verify(token, secret, (err, data) => {

			//if jwt is not valid
			if(err) {
				return res.send({auth: "failed: invalid token"})

			//if jwt if valid
			} else {
				//call a function
				//to proceed to the next middleware function/callback
				//in the route
				//sino yung kasunod na function, yun yung irrun
				next();
			}
		})
	//token does not exist
	} else {
		return res.send({auth: "failed: token does not exist"})
	};
}


//token decryption

//Analogy: open the gift and get the content

module.exports.decode = (token) => {
	
	//TYPE:  BEARER TOKEN
	//token received and is not undefine
	if (typeof token !== "undefined"){
		//removes the "Bearer" prefix
		token = token.slice(7, token.length);

		//verify method
		return jwt.verify(token, secret, (err,data) => {
			if (err) {
				return null;
			} else {
				//decode method
				//used to obtain info from the jwt
				// complete: true - called option, that will allow you
				//return additional info from the JWT token
				//payload - contains info provided in the "createAccessToken"
				// (id, email, isAdmin)
				return jwt.decode(token, {complete: true}).payload;
			}
		})
	} else {
		return null;
	}
}